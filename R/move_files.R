standards.move.files=function(standards_data,indir,outdir,hardlinks=FALSE){
  
  
  # make copy
  standards_data_new = standards_data
  
  
  # Path of input files
  files=list.files(indir,recursive=T,pattern=".mzData")
  file_base=str_replace(basename(files),".mzData","")
  
  
  # removed files marked for skipping
  standards_data_new = standards_data_new[  !(standards_data_new[,"skip"]==1)        ,]
  
  
  # get filesnames
  target_files=as.character(standards_data_new[ ,"filenames"])
  
  
  
  
  # Find files and copy
  missing=as.data.frame(matrix(nrow=0,ncol=5))
  
  for (i in 1:length(target_files)){
    select = target_files[i]==file_base
    
    if (sum(select)<1){
      #missing = c(missing,target_files[i]) 
      missing = rbind(missing,      cbind(filenames=as.character(standards_data_new[i,"filenames"]),namelist=as.character(standards_data_new[i,"namelist"]),type=as.character(standards_data_new[i,"type"]),method=as.character(standards_data_new[i,"method"]),mode=as.character(standards_data_new[i,"mode"])                           )       )  
      
    }else{
      
      # do hardlinks if selected
      if (hardlinks){
        file.link(from=paste(indir,files[select],sep="/"), to=paste(outdir,basename(files[select]),sep="/"))
      }else{
        file.copy(from=paste(indir,files[select],sep="/"), to=outdir, overwrite=F)
      }
      
      
    }  
  }
  
  
  # write file with missing files
  if(!(nrow(missing)==0)){
    write.xlsx2(missing[order(missing[,"filenames"]),],"missing_files.xlsx")
  }
  
  
  
  
  
  # Write file with files found but not considered
  all_out_files=list.files(outdir,recursive=T,pattern=".mzData")
  all_out_files_base=str_replace(basename(all_out_files),".mzData","")
  
  not_considered = is.na(match(file_base,all_out_files_base))
  not_considered = files[not_considered]
  
  write.xlsx2(not_considered,file="files_not_in_list.xlsx")
  
  
  
  # For the moment we just remove missing files
  if(!(nrow(missing)==0)){
  loc = sapply(as.character(missing[,"filenames"]), function(x) which(x==target_files))
  loc = unlist(loc)
  standards_data_new=standards_data_new[-loc,]
  }
  
  
  
  write.xlsx2(standards_data_new,file="list_of_standards_to_analyze.xlsx")
  
  
  return(standards_data_new)
  
}