# Auto Process Chemical Standards

This R package aims to make it simple and fast to analyze LC-MS data of chemical standards. The final output is a table of retention times (RT) and all _m/z_ value related to each chemical standard. Additional features include extraction of the chemical structure (InChI) from pubchem CIDs. The list of RT/_m/z_ can then be used to annotate metabolomics data (for example using the db.comp.assign function in [chemhelper](https://github.com/stanstrup/chemhelper)).


## Installation
```R
install.packages("devtools")
source("https://bioconductor.org/biocLite.R")
devtools::install_url("https://gitlab.com/R_packages/obabel2R/-/archive/master/obabel2R-master.zip")
devtools::install_url("https://gitlab.com/stanstrup-obsolete/CTSgetR/-/archive/master/CTSgetR-master.zip")
devtools::install_url("https://gitlab.com/R_packages/chemhelper/-/archive/master/chemhelper-master.zip", args="--no-multiarch")
devtools::install_url("https://gitlab.com/R_packages/APCS/-/archive/master/APCS-master.zip", args="--no-multiarch")
```

## Updating
```R
apcs.update()
```




## Step 1: Clean up a list of standards
This step takes a table looking like this:

| filenames      | namelist                                                                      | type       | method | mode | formula    | mass   | pubchem  | Inchi | comment | skip |
|----------------|-------------------------------------------------------------------------------|------------|--------|------|------------|--------|----------|-------|---------|------|
| 19032012-009   | (DL)-p-hydroxyphenyllactic acid                                               | MSE        | old    | neg  | C9H10O4    | 182.06 | 9378     |       |         |      |
| 19032012-013   | (DL)-p-hydroxyphenyllactic acid                                               | MS         | old    | pos  | C9H10O4    | 182.06 | 9378     |       |         |      |
| 2012-07-11-033 | (R)-2-hydroxybutyric acid                                                     | MS/MS_10eV | old    | neg  | C4H8O3     | 104.05 | 11266    |       |         |      |
| 2012-07-11-034 | (R)-2-hydroxybutyric acid                                                     | MS/MS_20eV | old    | neg  | C4H8O3     | 104.05 | 11266    |       |         |      |
| 2012-09-13-008 | 11-Deoxy-17-hydroxycorticosterone                                             | MS         | old    | pos  | C21H30O4   | 346.21 | 440707   |       |         |      |
| 15102012-010   | 1-O-1'-(Z)-octadecenyl-2-hydroxy-sn-glycero-3-phosphocholine (LysoPC(P-18:0)) | MS         | old    | neg  | C26H54NO6P | 507.37 | 24779527 |       |         |      |
| 15102012-009   | 1-O-1'-(Z)-octadecenyl-2-hydroxy-sn-glycero-3-phosphocholine (LysoPC(P-18:0)) | MS         | old    | pos  | C26H54NO6P | 507.37 | 24779527 |       |         |      |
| 15102012-007   | 20-beta-Hydroxy-5-alpha-pregnan-3-one                                         | MS         | old    | neg  | C21H34O2   | 318.26 | 3080558  |       |         |      |
| 15102012-012   | 20-beta-Hydroxy-5-alpha-pregnan-3-one                                         | MS/MS_20eV | old    | pos  | C21H34O2   | 318.26 | 3080558  |       |         |      |



The following processes are performed:

1. The compound names are changed based on the file "synonyms.xlsx". An example is supplied with the package. This is used to clean up incorrect or unfortunate compound names.
The example file can be found by:

  ```R
  system.file("extdata","synonyms.xlsx", package="APCS")
  ```

2. Based on the names pubchem is queried to attempt to find matching pubchem IDs. The result is written to file "suggested_pubchem.xlsx". The user should then manually inspect this file and copy the correct pubchem IDs to the initial list of standards.
3. All pubchem IDs are converted to InChIs.
4. Salts are removed from the structures (from the InChI).
5. The mass is calculated if not specified in the file. It is therefore not necessary to specify the mass if pubchem or InChI is specified. 
6. The result is written to "list_of_standards_cleaned.xlsx".

The InChI column should only be filled if no Pubchem ID exists.
The "skip" column can be used to specify lines that should be ignored. The "formula" column is not used for anything at the moment and can always be calculated from pubchem or InChI.
The "type" column is used to identify which files have MSE data. If MSE is specified the script will look for files with "MSE" in the filename in addition to the filename with no MSE.

The input file can be read from excel:
```R
standards_data=read.xlsx2('list_of_standards.xlsx',1,stringsAsFactors=F,colIndex=1:11)

```

Alternatively a file can be read from google docs so that the file can be updated by many. This unfortunately only works with older stylesheets (hint: copy an old one and copy in your new data).

```R
standards_data <- google_ss(gid = [SHEET NUMBER. STARTING FROM 0],             key = "[YOUR KEY]")

```


Then the cleanup function is simply applied.
```R
standards_data = standards.cleanup(standards_data)
```

The package includes an example scripts with the individual steps that are performed by "standards.cleanup".
This script can be found by running

```R
system.file("extdata","example_scripts/workflow.R", package="APCS")
```




## Step 2: Finding the files to process

1. The files listed in the file generated above are located from a root folder (recursively) and copied to another single folder. Files are not overwritten so this can be used to quickly copy new files. The copied files are hardlinked to save space if supported by the file system.
2. A list of files that could not be found is generated (missing_files.xlsx).
3. A list of files found in the root folder but not listed in the list of standards is generated (files_not_in_list.xlsx).
4. A list of standards as above is generated. But with the rows where the file could not be found removed.

```R
standards_data = read.xlsx2("list_of_standards_cleaned.xlsx",1) # Read list of compounds

indir="/[PATH]/Converted" # Root path to copy files FROM (recursive)
outdir="/[PATH]/selected_collection/" # Set place to copy files TO

standards_data = standards.move.files(standards_data,indir=indir,outdir=outdir,hardlinks=TRUE)
```




## Step 3: Analyzing the standards

All files are processed so that:

1. Peaks are extracted.
2. The peak corresponding to the compound that should be present is located. The file is skipped if no such peak is found.
3. All features strictly coeluting with the above peak are found. The Features are automatically annotated for fragments and adducts. So far this annotation is very much hit or miss because the annotation doesn't force the known pseudomolecular ion to be the basis for the annotation.
4. A table with all the data as shown below is written to the file "analysed.xlsx":


| Filename     | Method | Mode | Compound.name                   | Formula | Monoisotopic.mass | mz               | RT               | Annotation             | Pubchem | InChI                                                                         | Type          |
|--------------|--------|------|---------------------------------|---------|-------------------|------------------|------------------|------------------------|---------|-------------------------------------------------------------------------------|---------------|
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 428.026820140694 | 2.543            | [M-H+NaCl]- 371.1      | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 119.052907807296 | 2.55781666666667 | [M-H-H2O-CO2]- 182.059 | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 598.082875442971 | 2.5466           | [3M-4H+Fe3+]- 182.059  | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 599.088746721686 | 2.543            | [3M-3H+Fe2+]- 182.059  | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 583.120648507552 | 2.5542           | [3M-2H+K]- 182.059     | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 363.110290406171 | 2.5466           | [2M-H]- 182.059        | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 370.100404006275 | 2.55781666666667 | [M-H]- 371.1           | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 371.094234969058 | 2.543            |                        | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 385.092528569103 | 2.543            | [2M-2H+Na]- 182.059    | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 135.046064446573 | 2.5466           | [M-H-HCOOH]- 182.059   | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 416.023340378072 | 2.543            | [2M-4H+Fe3+]- 182.059  | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 163.04012788915  | 2.5466           | [M-H-H2O]- 182.059     | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-046 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 181.051925836473 | 2.5466           | [M-H]- 182.059         | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MS  |
| 19032012-050 | new    | neg  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | 181.051577953752 | 2.53653333333333 |                        | 9378    | InChI=1S/C9H10O4/c10-7-3-1-6(2-4-7)5-8(11)9(12)13/h1-4,8,10-11H,5H2,(H,12,13) | Standard, MSE |
| 19032012-055 | new    | pos  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | NaN              | NaN              | NaN                    | 9378    |                                                                               | Standard, MS  |
| 19032012-059 | new    | pos  | (DL)-p-hydroxyphenyllactic acid | C9H10O4 | 182.057907        | NaN              | NaN              | NaN                    | 9378    |                                                                               | Standard, MSE |





```R
standards_data = read.xlsx2("list_of_standards_to_analyze.xlsx",1)
indir="/[PATH]/selected_collection"

standards.analyze(standards_data,indir,ppm=30,ppm_MSMS=100)
```
